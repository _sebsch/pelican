Timeout decorator
==================

:author: sebsch
:date:  2018-08-18
:tags: pattern, decorator
:category: python 


Sometimes it makes sense to end a method when a defined point in time is exceeded.
The following code shows  a decorator that will end any decorated function after the defined time.


.. code-include:: /data/some_patterns/decorators/timed_task.py
    :lexer: python
    :encoding: utf-8
    :tab-width: 4
    
To use this we have just decorate a function and whait til the exception is thrown. 


.. code-include:: /data/some_patterns/decorators/use_timed_task.py
    :lexer: python
    :encoding: utf-8
    :tab-width: 4
    
The cool thing about this is the signal.alarm_ part. This function is able to call a so called handler after a defined
period of time. In this case this is the method handle_timeout wich will raise the Timeout-Exception.
    

.. code:: python

    decorators.timed_task.Timeout: Task TIMEOUT! [5 Seconds]
    
.. _signal.alarm: https://docs.python.org/3/library/signal.html
    

    

