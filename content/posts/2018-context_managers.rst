Git clone and delete after using
================================

:author: sebsch
:date:  2018-08-19
:tags: pattern, context-manager
:category: python 




Automatic changes in  a git-repository can cause merge-conflicts. Therefore it can be handy to temporary clone the remote, do your changes and delete the repo after the changes are pushed. The following code does this with the help of a context-manager.

.. code-include:: /data/some_patterns/context_manager/clone_and_delete.py
    :lexer: python
    :encoding: utf-8
    :tab-width: 4
