#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'sebsch'
SITENAME = u'notebook'
SITEURL = 'https://sebsch.is'

PATH = 'content'
OUTPUT_PATH = 'public'


TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'

# Blogroll
LINKS = (('online-cv (german)', 'https://cv.sebsch.is'),)

# Social widget
#SOCIAL = (None)

DEFAULT_PAGINATION = 5


## THEME Configuration

THEME = '/data/themes/pelican-alchemy/alchemy'
# Display pages list on the top menu

SITESUBTITLE = 'some usefull patterns and solutions'


PLUGIN_PATHS = ['/data/pelican-plugins']
PLUGINS = ['code_include', 'pelican-bootstrapify']

BOOTSTRAPIFY = {
    'table': ['table', 'table-striped', 'table-hover'],
    'img': ['img-fluid'],
    'blockquote': ['blockquote'],
}

ICONS = [
    ('gitlab', 'https://gitlab.com/_sebsch'),
    ('rss', 'feeds/all.atom.xml')
]

# Blogroll
LINKS 
# Social widget
#SOCIAL

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False
